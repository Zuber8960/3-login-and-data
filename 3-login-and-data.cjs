/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

//========================================================================================


// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


const fs = require("fs");


function createFiles(file, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, content, (err) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve(`file created successfully`);
            }
        });
    })
    .then(res => {
        return res
    })
    .catch(err => {
        console.error(err);
    });
};




function readFiles(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, "utf-8", (err, data) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve(data);
            }
        });
    })
    .then(res => {
        return res
    })
    .catch(err => {
        console.error(err);
    });
};

function deleteFiles(file) {
    return new Promise((resolve, reject) => {
        fs.unlink(file, (err) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve(`file deleted successfully`);
            }
        });
    })
    .then(res => {
        return res
    })
    .catch(err => {
        console.error(err);
    });
}


function createTwoFilesParellel() {
    return Promise.all([createFiles("./file1.txt"), createFiles("./file2.txt", "file2")])
}


function deleteFilesOneAfterAnother() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            deleteFiles("./file1.txt")
                .then((res) => {
                    console.log(res);
                    return deleteFiles("./file2.txt");
                })
                .then((res) => {
                    console.log(res);
                    resolve();
                })
                .catch(err => {
                    reject(new Error(err));
                })
        }, 2000);
    })
}

function problem1() {
    createTwoFilesParellel()
        .then((res) => {
            console.log(res);
            return deleteFilesOneAfterAnother();
        })
        .catch(err => {
            console.error(err);
        })

}

problem1();




// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining
// */


const lipsumUrl = "https://lipsum.com/feed/json";

function problem2() {
    fetch(lipsumUrl)
        .then(res => {
            return res.json();
        })
        .then(res => {
            // console.log(res);
            return createFiles("./lipsum.txt", res.feed.lipsum);
        })
        .then(res => {
            console.log(res);
            return readFiles("./lipsum.txt");
        })
        .then(res => {
            // console.log(res);
            return createFiles("./lipsumNewFile.txt", res);
        })
        .then(res => {
            console.log(res);
            return deleteFiles("./lipsum.txt");
        })
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.error(err);
        })
};

problem2();
